package ru.tsc.apozdnov.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.model.AbstractModel;


@Repository
public interface AbstractRepository<M extends AbstractModel> extends JpaRepository<M, String> {


}