package ru.tsc.apozdnov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;

@NoArgsConstructor
public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final TaskDtoModel task) {
        super(task);
    }

}