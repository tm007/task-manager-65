package ru.tsc.apozdnov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDtoModel user;

    public AbstractUserResponse(@Nullable UserDtoModel user) {
        this.user = user;
    }

}