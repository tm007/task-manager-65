package ru.tsc.apozdnov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.*;

@Repository
public class TaskRepository {

    @NotNull
    private final Map<String, Task> tasks = new HashMap<>();

    {
        add("Task-" + UUID.randomUUID());
        add("Task-" + UUID.randomUUID());
        add("Task-" + UUID.randomUUID());
    }


    public Task add(@NotNull final String name) {
        final Task task = new Task(name);
        return tasks.put(task.getId(), task);
    }

    public Task add(@NotNull final Task task) {
        return tasks.put(task.getId(), task);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    @NotNull
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @NotNull
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public Task save(@NotNull final Task task) {
        return tasks.put(task.getId(), task);
    }

    public void remove(@NotNull final Task task) {
        tasks.remove(task.getId());
    }

    public void remove(@NotNull final List<Task> tasks) {
        tasks.forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return tasks.containsKey(id);
    }

    public void clear() {
        tasks.clear();
    }

    public long count() {
        return tasks.size();
    }


}