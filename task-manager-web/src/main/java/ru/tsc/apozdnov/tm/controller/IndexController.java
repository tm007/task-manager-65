package ru.tsc.apozdnov.tm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/docs/api")
    public String indexdocs() {
        return "target/docs/rest/index.html";
    }

}