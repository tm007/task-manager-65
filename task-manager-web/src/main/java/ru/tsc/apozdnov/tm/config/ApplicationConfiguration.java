package ru.tsc.apozdnov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.apozdnov.tm")
public class ApplicationConfiguration {
}