package ru.tsc.apozdnov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.apozdnov.tm.api.TaskRestEndpoint;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskRestEndpointClient implements TaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(taskRepository.findAll());
    }

    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    @PostMapping("/save")
    public Task save(@NotNull @RequestBody final Task task) {
        return taskRepository.add(task);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final Task task) {
        taskRepository.remove(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<Task> tasks) {
        taskRepository.remove(tasks);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        taskRepository.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        taskRepository.removeById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskRepository.count();
    }
}