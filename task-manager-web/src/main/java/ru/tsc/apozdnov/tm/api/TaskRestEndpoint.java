package ru.tsc.apozdnov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;

@RequestMapping("/api/task")
public interface TaskRestEndpoint {

    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/findById/{id}")
    Task findById(@NotNull @PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@NotNull @PathVariable("id") String id);

    @PostMapping("/save")
    Task save(@NotNull @RequestBody Task task);

    @PostMapping("/delete")
    void delete(@NotNull @RequestBody Task task);

    @PostMapping("/deleteAll")
    void clear(@NotNull @RequestBody List<Task> tasks);

    @PostMapping("/clear")
    void clear();

    @PostMapping("/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @GetMapping("/count")
    long count();

}