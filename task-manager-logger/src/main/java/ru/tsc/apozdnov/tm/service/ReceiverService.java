package ru.tsc.apozdnov.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.apozdnov.tm.api.service.IReceiverService;

import javax.jms.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ReceiverService implements IReceiverService {

    @NotNull
    private static final String TOPIC_NAME = "LOG_TOPIC";

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @Override
    @SneakyThrows
    public void receiveLog(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(TOPIC_NAME);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}